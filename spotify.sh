#!/usr/bin/env bash

### Helpers functions
echo -e "\n\e[1m# 0. Download script dependencies...\e[0m"
source <(wget -qO - https://bitbucket.org/nojes/install/raw/master/.helpers.sh)
echo "done."

### Main script
APP=Spotify

main()
{
    echo_bold "Installing ${APP} ..."

    action "Add the ${APP} repository signing keys to be able to verify downloaded packages..."
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0DF731E45CE24F27EEEB1450EFDC8610341D9410

    action "Add the ${APP} repository..."
    echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list

    action "Update list of available packages..."
    sudo apt-get update &> /dev/null
    echo "done."

    action "Install ${APP}..."
    sudo apt-get install -y spotify-client

    echo_success "Done."
}

main

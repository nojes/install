# install

> The simple quick installation shell scripts.

### [Spotify](https://www.spotify.com)
```bash
$ wget -O - https://bitbucket.org/nojes/install/raw/master/spotify.sh | sudo bash
```

### [Timecamp](https://www.timecamp.com)
```bash
$ wget -O - https://bitbucket.org/nojes/install/raw/master/timecamp.sh | sudo bash
```

### [java-non-latin-shortcuts](https://ru.stackoverflow.com/questions/469241/%D0%9F%D1%80%D0%BE%D0%B1%D0%BB%D0%B5%D0%BC%D0%B0-%D1%81-%D0%B1%D1%8B%D1%81%D1%82%D1%80%D1%8B%D0%BC%D0%B8-%D0%BA%D0%BB%D0%B0%D0%B2%D0%B8%D1%88%D0%B0%D0%BC%D0%B8-%D0%B2-%D0%BD%D0%B5-%D0%B0%D0%BD%D0%B3%D0%BB%D0%B8%D0%B9%D1%81%D0%BA%D0%BE%D0%B9-%D1%80%D0%B0%D1%81%D0%BA%D0%BB%D0%B0%D0%B4%D0%BA%D0%B5-%D0%B2-ubuntu)
```bash
$ wget -O - https://bitbucket.org/nojes/install/raw/master/java-non-latin-shortcuts.sh | sudo bash
```

#!/usr/bin/env bash

NORMAL='\e[0m'
GREEN='\e[32m'
YELLOW='\e[33m'
RED='\e[31m'
BOLD='\e[1m'

action_count=0

echo_n()
{
    echo -e "\n${1}${NORMAL}"
}

echo_bold()
{
    echo_n "${BOLD}${1}"
}

echo_success()
{
    echo_bold "${GREEN}${1}"
}

echo_warning()
{
    echo_bold "${YELLOW}${1}"
}

echo_error()
{
    echo_bold "${RED}${1}"
}

action()
{
    action_count=$((action_count + 1))

    echo_bold "# $action_count. $1"
}

action_apt_update()
{
    action "Update list of available packages..."
    sudo apt-get update &> /dev/null
    echo "done."
}

#!/usr/bin/env bash

### Helpers functions
echo -e "\n\e[1m# 0. Download script dependencies...\e[0m"
source <(wget -qO - https://bitbucket.org/nojes/install/raw/master/.helpers.sh)
echo "done."

### Main script
APP=Timecamp
DEB_FILE=timecamp.deb
LINK="https://www.timecamp.com/downloadsoft/timecamp_1.3.8.4_amd64.deb"

main()
{
    echo_bold "Installing ${APP} ..."

    action "Install ${APP} dependencies..."
    sudo apt-get install -y \
        libc-bin \
        python \
        libappindicator1 \
        libcurl3 \
        libdbus-1-3 \
        libgtk2.0-0 \
        libindicator7 \
        liblzma5 \
        libnotify4 \
        libsqlcipher0 \
        libsqlite3-0 \
        libudev1 \
        libx11-6 \
        libxss1 \
        libpng16-16

    action "Download ${APP}..."
    wget -O ${DEB_FILE} ${LINK}

    action "Install ${DEB_FILE}..."
    sudo dpkg -i ${DEB_FILE}

    action "Cleaning..."
    rm ${DEB_FILE}

    echo_success "Done."
}

main

#!/usr/bin/env bash

### Helpers functions
echo -e "\n\e[1m# 0. Download script dependencies...\e[0m"
source <(wget -qO - https://bitbucket.org/nojes/install/raw/master/.helpers.sh)
echo "done."

### Main script
APP=java-non-latin-shortcuts

main()
{
    echo_bold "Installing ${APP} ..."

    action "Add the ${APP} repository..."
    sudo add-apt-repository ppa:attente/java-non-latin-shortcuts

    action_apt_update

    action "Dist upgrade..."
    sudo apt-get dist-upgrade

    action "restart unity-settings-daemon"
    restart unity-settings-daemon

    echo_success "Done."
}

main

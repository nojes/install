#!/usr/bin/env bash
##
# Creates scripts by `.template`
#
# Usage:
# ./_create.sh app
#

APP=${1:-app}
TEMPLATE=${2:-.template}
GREEN="\e[32m"
script_file="${APP}.sh"

render() {
  eval "echo \"$(cat $1)\""
}

render ./.template > ${script_file}
chmod +x ${script_file}

echo -e "${GREEN}Created ${script_file}\n"
